package com.atlassian.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AccessLogger {
    private static final Logger LOG = LoggerFactory.getLogger(AccessLogger.class);

    public static final ServletFilter filter = (Servlet inner) ->
            (final HttpServletRequest req, final HttpServletResponse resp) -> {
                LOG.info(
                        "Request {}: {}",
                        req.getMethod(),
                        req.getRequestURI()
                );
                inner.handle(req, resp);
                LOG.info(
                        "Response {}: {} ({})",
                        req.getMethod(),
                        req.getRequestURI(),
                        resp.getStatus()
                );
            };
}