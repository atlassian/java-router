package com.atlassian.http.router;

import java.util.Map;

public class WithParams {
    public WithParams(Map<String, String> params) {
        this.params = params;
    }

    public final Map<String, String> params;

    String getParam(String key) {
        return params.get(key);
    }

}
