package com.atlassian.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ErrorLogger {
    private static final Logger LOG = LoggerFactory.getLogger(ErrorLogger.class);

    public static final ServletFilter filter = (Servlet inner) ->
            (final HttpServletRequest req, final HttpServletResponse resp) -> {
                try {
                    inner.handle(req, resp);
                } catch (Exception e) {
                    LOG.error("Error escaped the filter stack: {}", e);
                }
            };

}
