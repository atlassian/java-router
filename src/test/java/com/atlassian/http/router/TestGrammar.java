package com.atlassian.http.router;

import com.atlassian.parsers.routing.path.routerpathLexer;
import org.antlr.v4.runtime.Token;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static com.atlassian.http.router.Helpers.*;


public class TestGrammar {

    private ExpectedException thrown = ExpectedException.none();

    @Rule
    public TestRule rules = RuleChain.emptyRuleChain().around(thrown);


    @Test
    public void testLexer() throws Exception {
        List<Token> tokens = lex("//alpha/beta/gamma/3234/:4345/*sam");

        int i = 0;
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.SLASH));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.SLASH));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.WORD));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.SLASH));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.WORD));

        // 5
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.SLASH));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.WORD));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.SLASH));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.NUMBER));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.SLASH));

        // 10
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.PARAM));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.NUMBER));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.SLASH));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.REST));
        assertThat(tokens.get(i++).getType(), is(routerpathLexer.WORD));

        // 15
        assertThat(tokens.get(i++).getType(), is(Token.EOF));
    }

    @Test
    public void testParserConsumesAllTokensOrErrors() throws Exception {
        thrown.expect(ParseException.class);
        parse("//");
    }

    @Test
    public void testParser() throws Exception {
        Tree t = parse("/aword");
        assertThat(
                t,
                equalTo(tree(
                        "<root>",
                        tree("/"),
                        tree("pathElems", tree("aword"))
                ))
        );
    }


}

